<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Http\Requests\StoreShopRequest;
use App\Http\Requests\UpdateShopRequest;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use stdClass;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::withCount('products')->get();
        return $shops;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreShopRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShopRequest $request)
    {
        try {
            $validated = $request->validated();
            DB::beginTransaction();

            $shop = Shop::saveIfNotExists($validated['name']);
            foreach($validated['products'] as $p){
                /** @var Product */
                $product = Product::saveIfNotExists($p['name']); 
                if($shop->products->contains($product->id)){
                    $shop->products()->detach($product->id);
                }
                $shop->products()->attach($product->id, ['amount' => $p['amount']]);
            }

            DB::commit();
            return redirect(route('shops.show', $shop->id));
        } 
        catch (\Exception $ex) {
            DB::rollBack();
            return $ex;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        return $shop->getProductsAmount();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateShopRequest  $request
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShopRequest $request, Shop $shop)
    {
        try {
            DB::beginTransaction();

            $validated = $request->validated();

            $shop = Shop::saveIfNotExists($validated['name']);

            foreach($validated['products'] as $p){
                /** @var Product */
                $product = Product::saveIfNotExists($p['name']); 
                if($shop->products->contains($product->id)){
                    $shop->products()->detach($product->id);
                }
                $shop->products()->attach($product->id, ['amount' => $p['amount']]);

                
            }

            DB::commit();

            return redirect(route('shops.show', $shop->id));

        } catch (\Exception $ex) {
            DB::rollBack();
            return $ex;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {
        $shop->products()->detach();
        $shop->delete();

        return response()->json([], 204);
    }

    public function sell(Shop $shop, Product $product)
    {
        $newAmount = $shop->products()->find($product->id)->pivot->amount -1 ?? 0;

        if($newAmount < 0){
            return response()->json([
                'message' => 'Out of stock, impossible buying',
                'shop' => $shop->getProductsAmount()
            ]);
        }

        $shop->products()->updateExistingPivot($product->id, ['amount' => $newAmount ]);
        
        if($shop->products()->find($product->id)->pivot->amount == 1)
        {
            return response()->json([
                'message' => 'Product sold, but we have only one unit more in stock!',
                'shop' => $shop->getProductsAmount()
            ]);
        }
        return response()->json([
            'message' => "Product sold! $newAmount units left.",
            'shop' => $shop->getProductsAmount()
        ]);
    }
}
