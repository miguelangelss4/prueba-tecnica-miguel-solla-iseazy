<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $hidden = ['pivot'];
    protected $fillable = ['name'];

    /**
     * Las tiendas que tienen este producto. 
     */
    public function shops()
    {
        return $this->belongsToMany(Shop::class); //Aprovecho las convenciones de Laravel para no tener que especificar nombres de tabla ni campos en la relación
    }

    /**
     * Grabar producto si no existe. 
     * 
     * @var string $name Nombre de la producto a crear
     * 
     * @return Product Producto creada si no existía, o la encontrado si ya había uno con ese nombre
     */
    static function saveIfNotExists(string $name): Product
    {
        /** @var Product */
        $product = Product::where('name', $name)->first();
        if($product === null)
        {
            $product = new Product([ 'name' => $name ]);
            $product->save();
        }

        return $product;
    }
}
