<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $hidden = ['pivot'];
    protected $fillable = ['name'];
    
    /**
     * Los productos que vende esta tienda. 
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('amount'); //Aprovecho las convenciones de Laravel para no tener que especificar nombres de tabla ni campos en la relación
    }
    
    /**
     * Grabar tienda si no existe. 
     * 
     * @var string $name Nombre de la tienda a crear
     * 
     * @return Shop Tienda creada si no existía, o la encontrada si ya había una con ese nombre
     */
    static function saveIfNotExists(string $name): Shop
    {
        /** @var Shop */
        $shop = Shop::where('name', $name)->first();
        if($shop === null)
        {
            $shop = new Shop([ 'name' => $name ]);
            $shop->save();
        }

        return $shop;
    }

    static function checkIfExists(string $name): bool
    {
        return Shop::where('name', $name)->first() != null;
    }

    public function getProductsAmount()
    {
        foreach($this->products as $product){//get the product amount by each shop
            $product->amount = $product->pivot->amount;
        }
        return $this;
    }
}
