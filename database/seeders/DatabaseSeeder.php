<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Shop;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Shop::factory(50)->create();
        Product::factory(2000)->create();

        for ($i=0; $i < 200; $i++) { 
            try {
                $product = Product::inRandomOrder()->first();
                $shop = Shop::inRandomOrder()->first();
                DB::table('product_shop')->insert(
                    [
                        'product_id' => $product->id,
                        'shop_id'    => $shop->id,
                        'amount'     => rand(0, 5)
                    ]
                );
            } catch (QueryException $qe) {
                //ignorar posibles claves primarias duplicadas
            }
        }
        
    }
}
